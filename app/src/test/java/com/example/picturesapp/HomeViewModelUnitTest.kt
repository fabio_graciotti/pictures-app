package com.example.picturesapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.picturesapp.model.Album
import com.example.picturesapp.repository.HomeRepository
import com.example.picturesapp.ui.home.HomeViewModel
import com.example.picturesapp.utils.LifeCycleTestOwner
import com.example.picturesapp.utils.Resource
import com.example.picturesapp.utils.Status
import com.example.picturesapp.utils.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import java.lang.Exception

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelUnitTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var homeViewModel: HomeViewModel

    private val homeRepository: HomeRepository = mock()

    private val responseAlbumObserver: Observer<Resource<Response<List<Album>>>> = mock()

    private lateinit var lifeCycleTestOwner: LifeCycleTestOwner

    @Before
    fun setUp() {
        lifeCycleTestOwner = LifeCycleTestOwner()
        lifeCycleTestOwner.onCreate()
        homeViewModel = HomeViewModel(homeRepository)
        homeViewModel.responseAlbums.observe(lifeCycleTestOwner, responseAlbumObserver)
    }


    @Test
    fun `test getAlbums should return success with list size greater then 0`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData = Response.success(listOf(Album(1, 1, "Title")))

            // When
            whenever(homeRepository.getAlbums()).thenReturn(respData)

            homeViewModel.getAlbums()

            // Then
            verify(responseAlbumObserver).onChanged(Resource.loading())
            verify(responseAlbumObserver).onChanged(Resource.success(respData))

            assert(homeViewModel.responseAlbums.value?.data?.body()?.size!! > 0)

        }

    @Test
    fun `test getAlbums should return success with empty list`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData = Response.success(listOf<Album>())

            // When
            whenever(homeRepository.getAlbums()).thenReturn(respData)

            homeViewModel.getAlbums()

            // Then
            verify(responseAlbumObserver).onChanged(Resource.loading())
            verify(responseAlbumObserver).onChanged(Resource.success(respData))

            assert(homeViewModel.responseAlbums.value?.data?.body()?.size!! == 0)

        }

    @Test
    fun `test getAlbums should return success with 401 response code`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData = Response.error<List<Album>>(
                401,
                "Unauthorized".toResponseBody("text/plain".toMediaTypeOrNull())
            )

            // When
            whenever(homeRepository.getAlbums()).thenReturn(respData)

            homeViewModel.getAlbums()

            // Then
            verify(responseAlbumObserver).onChanged(Resource.loading())
            verify(responseAlbumObserver).onChanged(Resource.success(respData))

            assert(homeViewModel.responseAlbums.value?.data?.code() == 401)

        }


    @Test(expected = Exception::class)
    fun `test getAlbums should return error`() = testCoroutineRule.runBlockingTest {
        lifeCycleTestOwner.onResume()
        // Given
        val exception = Exception()

        // When
        whenever(homeRepository.getAlbums()).thenThrow(exception)
        homeViewModel.getAlbums()

        // Then
        verify(responseAlbumObserver).onChanged(Resource.loading())
        verify(responseAlbumObserver).onChanged(Resource.error(exception))
    }

    @After
    fun tearDown() {
        lifeCycleTestOwner.onDestroy()
        homeViewModel.responseAlbums.removeObserver(responseAlbumObserver)
    }
}