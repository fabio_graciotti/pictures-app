package com.example.picturesapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.picturesapp.model.Photo
import com.example.picturesapp.repository.GalleryRepository
import com.example.picturesapp.ui.gallery.GalleryViewModel
import com.example.picturesapp.utils.LifeCycleTestOwner
import com.example.picturesapp.utils.Resource
import com.example.picturesapp.utils.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import java.lang.Exception


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GalleryViewModelUnitTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var galleryViewModel: GalleryViewModel

    private val galleryRepository: GalleryRepository = mock()


    private val responsePhotoObserver: Observer<Resource<Response<List<Photo>>>> = mock()

    private lateinit var lifeCycleTestOwner: LifeCycleTestOwner

    @Before
    fun setUp() {
        lifeCycleTestOwner = LifeCycleTestOwner()
        lifeCycleTestOwner.onCreate()
        galleryViewModel = GalleryViewModel(galleryRepository)
        galleryViewModel.responsePhoto.observe(lifeCycleTestOwner, responsePhotoObserver)
    }

    @Test
    fun `test getPhoto should return success with list size greater then 0`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData =
                Response.success(listOf(Photo(1, 1, "Title", "http://thumbnaiUr", "http://url")))

            //When
            whenever(galleryRepository.getPhotosByAlbum(1)).thenReturn(respData)

            galleryViewModel.getPhoto(1)

            //Then
            verify(responsePhotoObserver).onChanged(Resource.loading())
            verify(responsePhotoObserver).onChanged(Resource.success(respData))

            assert(galleryViewModel.responsePhoto.value?.data?.body()?.size!! > 0)
        }

    @Test
    fun `test getPhoto should return success with empty list`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData = Response.success(listOf<Photo>())

            //When
            whenever(galleryRepository.getPhotosByAlbum(1)).thenReturn(respData)

            galleryViewModel.getPhoto(1)

            //Then
            verify(responsePhotoObserver).onChanged(Resource.loading())
            verify(responsePhotoObserver).onChanged(Resource.success(respData))

            assert(galleryViewModel.responsePhoto.value?.data?.body()?.size!! == 0)
        }

    @Test
    fun `test getPhoto should return success with 401 response code`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()
            // Given
            val respData = Response.error<List<Photo>>(
                401,
                "Unauthorized".toResponseBody("text/plain".toMediaTypeOrNull())
            )

            //When
            whenever(galleryRepository.getPhotosByAlbum(1)).thenReturn(respData)

            galleryViewModel.getPhoto(1)

            //Then
            verify(responsePhotoObserver).onChanged(Resource.loading())
            verify(responsePhotoObserver).onChanged(Resource.success(respData))

            assert(galleryViewModel.responsePhoto.value?.data?.code() == 401)
        }

    @Test(expected = Exception::class)
    fun `test getAlbums should return error`() = testCoroutineRule.runBlockingTest {
        lifeCycleTestOwner.onResume()
        // Given
        val exception = Exception()
        val anyInt = anyInt()

        // When
        whenever(galleryRepository.getPhotosByAlbum(anyInt)).thenThrow(exception)

        galleryViewModel.getPhoto(anyInt)

        // Then
        verify(responsePhotoObserver).onChanged(Resource.loading())
        verify(responsePhotoObserver).onChanged(Resource.error(exception))
    }


    @After
    fun tearDown() {
        lifeCycleTestOwner.onDestroy()
        galleryViewModel.responsePhoto.removeObserver(responsePhotoObserver)
    }
}