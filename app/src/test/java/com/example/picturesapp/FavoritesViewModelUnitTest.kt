package com.example.picturesapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.picturesapp.model.PhotoDb
import com.example.picturesapp.repository.FavoritesRepository
import com.example.picturesapp.ui.favorites.FavoritesViewModel
import com.example.picturesapp.utils.LifeCycleTestOwner
import com.example.picturesapp.utils.Resource
import com.example.picturesapp.utils.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class FavoritesViewModelUnitTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var favoritesViewModel: FavoritesViewModel

    private val favoritesRepository: FavoritesRepository = mock()

    private val responseFavoritesObserver: Observer<Resource<List<PhotoDb>>> = mock()

    private val responseOnDeleteObserver: Observer<Resource<Boolean>> = mock()

    private lateinit var lifeCycleTestOwner: LifeCycleTestOwner

    @Before
    fun setUp() {
        lifeCycleTestOwner = LifeCycleTestOwner()
        lifeCycleTestOwner.onCreate()

        favoritesViewModel = FavoritesViewModel(favoritesRepository)

        favoritesViewModel.favoritesResponse.observe(lifeCycleTestOwner, responseFavoritesObserver)
        favoritesViewModel.onDeleteResponse.observe(lifeCycleTestOwner, responseOnDeleteObserver)
    }

    @Test
    fun `test getAll should return success with list size greater then 0`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = listOf(PhotoDb(1, "Title", "http://url", "http://thumbnailUrl"))

            // When
            whenever(favoritesRepository.getAll()).thenReturn(data)

            favoritesViewModel.getAll()
            //Then
            verify(responseFavoritesObserver).onChanged(Resource.loading())
            verify(responseFavoritesObserver).onChanged(Resource.success(data))

            assert(favoritesViewModel.favoritesResponse.value?.data?.size!! > 0)
        }

    @Test
    fun `test getAll should return success with empty list`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = listOf<PhotoDb>()

            // When
            whenever(favoritesRepository.getAll()).thenReturn(data)

            favoritesViewModel.getAll()
            //Then
            verify(responseFavoritesObserver).onChanged(Resource.loading())
            verify(responseFavoritesObserver).onChanged(Resource.success(data))

            assert(favoritesViewModel.favoritesResponse.value?.data?.size!! == 0)
        }

    @Test(expected = Exception::class)
    fun `test getAll should return error`() = testCoroutineRule.runBlockingTest {
        lifeCycleTestOwner.onResume()
        // Given
        val exception = Exception()

        // When
        whenever(favoritesRepository.getAll()).thenThrow(exception)

        favoritesViewModel.getAll()


        //Then
        verify(responseFavoritesObserver).onChanged(Resource.loading())
        verify(responseFavoritesObserver).onChanged(Resource.error(exception))
    }

    @Test
    fun `test deleteAll should return success`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = listOf(PhotoDb(1, "Title", "http://url", "http://thumbnailUrl"))

            // When
            whenever(favoritesRepository.deleteAll(data)).thenReturn(Unit)

            favoritesViewModel.deleteAll(data)

            // Then
            //Then
            verify(responseOnDeleteObserver).onChanged(Resource.loading())
            verify(responseOnDeleteObserver).onChanged(Resource.success(true))

        }

    @Test(expected = Exception::class)
    fun `test deleteAll should return error`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = listOf(PhotoDb(1, "Title", "http://url", "http://thumbnailUrl"))
            val exception = Exception()
            // When
            whenever(favoritesRepository.deleteAll(data)).thenThrow(exception)

            favoritesViewModel.deleteAll(data)

            // Then
            //Then
            verify(responseOnDeleteObserver).onChanged(Resource.loading())
            verify(responseOnDeleteObserver).onChanged(Resource.error(exception))

        }

    @After
    fun tearDown() {
        lifeCycleTestOwner.onDestroy()
        favoritesViewModel.favoritesResponse.removeObserver(responseFavoritesObserver)
        favoritesViewModel.onDeleteResponse.removeObserver(responseOnDeleteObserver)
    }
}