package com.example.picturesapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.picturesapp.model.Photo
import com.example.picturesapp.repository.FullscreenRepository
import com.example.picturesapp.ui.fullscreen.FullscreenViewModel
import com.example.picturesapp.utils.LifeCycleTestOwner
import com.example.picturesapp.utils.Resource
import com.example.picturesapp.utils.TestCoroutineRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class FullscreenViewModelUnitTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var fullscreenViewModel: FullscreenViewModel

    private val fullscreenRepository: FullscreenRepository = mock()

    private val insertToFavoritesResponseObserver: Observer<Resource<Boolean>> = mock()
    private val onDeleteResponseObserver: Observer<Resource<Photo>> = mock()


    private lateinit var lifeCycleTestOwner: LifeCycleTestOwner

    @Before
    fun setUp() {
        lifeCycleTestOwner = LifeCycleTestOwner()
        lifeCycleTestOwner.onCreate()

        fullscreenViewModel = FullscreenViewModel(fullscreenRepository)
        fullscreenViewModel.insertToFavoriteResponse.observe(
            lifeCycleTestOwner,
            insertToFavoritesResponseObserver
        )
        fullscreenViewModel.onDeleteResponse.observe(lifeCycleTestOwner, onDeleteResponseObserver)
    }

    @Test
    fun `test insertPhotoToFavorite should return success`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = Photo(1, 1, "Title", "http://thumbnailUrl", "http://url")

            // When
            whenever(fullscreenRepository.insert(data.toPhotoDb())).thenReturn(Unit)

            fullscreenViewModel.insertPhotoToFavorite(data)

            // Then
            verify(insertToFavoritesResponseObserver).onChanged(Resource.loading())
            verify(insertToFavoritesResponseObserver).onChanged(Resource.success(true))
        }

    @Test(expected = Exception::class)
    fun `test insertPhotoToFavorite should return error`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = Photo(1, 1, "Title", "http://thumbnailUrl", "http://url")
            val exception = Exception()

            // When
            whenever(fullscreenRepository.insert(data.toPhotoDb())).thenThrow(exception)

            fullscreenViewModel.insertPhotoToFavorite(data)

            // Then
            verify(insertToFavoritesResponseObserver).onChanged(Resource.loading())
            verify(insertToFavoritesResponseObserver).onChanged(Resource.error(exception))
        }

    @Test
    fun `test deletePhoto should return success`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = Photo(1, 1, "Title", "http://thumbnailUrl", "http://url")

            // When
            whenever(fullscreenRepository.delete(data.toPhotoDb())).thenReturn(Unit)

            fullscreenViewModel.deletePhoto(data)

            // Then
            verify(onDeleteResponseObserver).onChanged(Resource.loading())
            verify(onDeleteResponseObserver).onChanged(Resource.success(data))

            assert(fullscreenViewModel.onDeleteResponse.value?.data == data)
        }

    @Test(expected = Exception::class)
    fun `test deletePhoto should return error`() =
        testCoroutineRule.runBlockingTest {
            lifeCycleTestOwner.onResume()

            // Given
            val data = Photo(1, 1, "Title", "http://thumbnailUrl", "http://url")
            val exception = Exception()

            // When
            whenever(fullscreenRepository.delete(data.toPhotoDb())).thenThrow(exception)

            fullscreenViewModel.deletePhoto(data)

            // Then
            verify(insertToFavoritesResponseObserver).onChanged(Resource.loading())
            verify(insertToFavoritesResponseObserver).onChanged(Resource.error(exception))
        }

    @After
    fun tearDown() {
        lifeCycleTestOwner.onDestroy()
        fullscreenViewModel.insertToFavoriteResponse.removeObserver(
            insertToFavoritesResponseObserver
        )
        fullscreenViewModel.onDeleteResponse.removeObserver(onDeleteResponseObserver)
    }

}