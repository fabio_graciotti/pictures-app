package com.example.picturesapp.api

import com.example.picturesapp.costants.ServerDomain
import com.example.picturesapp.model.Album
import com.example.picturesapp.model.Photo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET(ServerDomain.ALBUMS)
    suspend fun getAlbums(): Response<List<Album>>

    @GET(ServerDomain.ALBUM_PHOTOS)
    suspend fun getPhotosByAlbum(@Path("id") albumId: Int): Response<List<Photo>>
}