package com.example.picturesapp.ui.favorites.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.example.picturesapp.R
import com.example.picturesapp.model.PhotoDb
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.squareup.picasso.Picasso

class FavoriteViewHolder(parent: ViewGroup?) :
    BaseViewHolder<PhotoDb>(parent, R.layout.favorite_item_layout) {
    private val titleTextView: TextView
    private val imageView: ImageView
    private val checkBox: CheckBox

    init {
        titleTextView = `$`(R.id.titleTextView)
        imageView = `$`(R.id.imageView)
        checkBox = `$`(R.id.checkBox)
    }

    override fun setData(data: PhotoDb?) {
        data?.let { photoDb ->
            titleTextView.text = photoDb.title
            Picasso.get().load(photoDb.thumbnailUrl).into(imageView)
            checkBox.isChecked = photoDb.isChecked
            checkBox.visibility = if (photoDb.isCheckable) View.VISIBLE else View.GONE
        }
    }
}