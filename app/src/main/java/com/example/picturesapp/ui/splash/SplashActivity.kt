package com.example.picturesapp.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.picturesapp.MainActivity
import com.example.picturesapp.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val mainIntent = Intent(this, MainActivity::class.java)
        // use the coroutine to emulate data loading
        GlobalScope.launch {
            delay(500)
            startActivity(mainIntent)
            finish()
        }
    }
}