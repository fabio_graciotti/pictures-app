package com.example.picturesapp.ui.component

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.picturesapp.R

class BottomGridItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        var flag1 = parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount - 1
        if (parent.adapter!!.itemCount % 2 == 0) {
            flag1 = flag1 || parent.getChildAdapterPosition(
                view
            ) == parent.adapter!!.itemCount - 2
        }
        val flag2 = parent.getChildAdapterPosition(view) % 2 == 0
        if (flag1)
            outRect.bottom = view.context.resources.getDimensionPixelSize(R.dimen.bottom_margin)
        if (flag2)
            outRect.right =
                view.context.resources.getDimensionPixelSize(R.dimen.item_end_margin)
        if (!flag1 && !flag2)
            super.getItemOffsets(outRect, view, parent, state)
    }
}