package com.example.picturesapp.ui.favorites.adapter

import android.content.Context
import android.view.ViewGroup
import com.example.picturesapp.model.PhotoDb
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class FavoriteAdapter(context: Context) : RecyclerArrayAdapter<PhotoDb>(context) {
    override fun OnCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder<*> {
        return FavoriteViewHolder(parent)
    }
}