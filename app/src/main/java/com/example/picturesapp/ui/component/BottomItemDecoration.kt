package com.example.picturesapp.ui.component

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.picturesapp.R

class BottomItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount - 1)
            outRect.bottom = view.context.resources.getDimensionPixelSize(R.dimen.bottom_margin)
        else
            super.getItemOffsets(outRect, view, parent, state)
    }
}