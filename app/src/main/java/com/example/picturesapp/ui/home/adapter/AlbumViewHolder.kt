package com.example.picturesapp.ui.home.adapter

import android.view.ViewGroup
import android.widget.TextView
import com.example.picturesapp.R
import com.example.picturesapp.model.Album
import com.jude.easyrecyclerview.adapter.BaseViewHolder

class AlbumViewHolder(parent: ViewGroup?) :
    BaseViewHolder<Album>(parent, R.layout.album_item_layout) {
    private val titleAlbumTextView: TextView

    init {
        titleAlbumTextView = `$`(R.id.titleAlbumTextView)
    }

    override fun setData(data: Album?) {
        data?.let { album ->
            titleAlbumTextView.text = album.title
        }
    }
}