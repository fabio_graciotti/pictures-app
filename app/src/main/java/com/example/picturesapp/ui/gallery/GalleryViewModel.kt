package com.example.picturesapp.ui.gallery

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.picturesapp.model.Photo
import com.example.picturesapp.repository.GalleryRepository
import com.example.picturesapp.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception
import javax.inject.Inject

class GalleryViewModel @Inject constructor(
    private val galleryRepository: GalleryRepository
) : ViewModel() {
    private val _responsePhoto: MutableLiveData<Resource<Response<List<Photo>>>> by lazy {
        MutableLiveData<Resource<Response<List<Photo>>>>()
    }
    val responsePhoto get() = _responsePhoto

    fun getPhoto(albumId: Int) {
        viewModelScope.launch {
            _responsePhoto.postValue(Resource.loading())
            try {
                val resp = galleryRepository.getPhotosByAlbum(albumId)
                _responsePhoto.postValue(Resource.success(resp))
            } catch (e: Exception) {
                _responsePhoto.postValue(Resource.error(e))
            }
        }
    }
}