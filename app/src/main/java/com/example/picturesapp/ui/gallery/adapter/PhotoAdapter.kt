package com.example.picturesapp.ui.gallery.adapter

import android.content.Context
import android.view.ViewGroup
import com.example.picturesapp.model.Photo
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class PhotoAdapter(context: Context) : RecyclerArrayAdapter<Photo>(context) {
    override fun OnCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder<*> {
        return PhotoViewHolder(parent)
    }
}