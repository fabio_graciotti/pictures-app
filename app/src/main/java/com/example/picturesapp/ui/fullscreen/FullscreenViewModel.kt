package com.example.picturesapp.ui.fullscreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.picturesapp.model.Photo
import com.example.picturesapp.repository.FullscreenRepository
import com.example.picturesapp.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class FullscreenViewModel @Inject constructor(
    private val repository: FullscreenRepository
) : ViewModel() {
    private val _insertToFavoriteResponse: MutableLiveData<Resource<Boolean>> by lazy {
        MutableLiveData<Resource<Boolean>>()
    }
    val insertToFavoriteResponse get() = _insertToFavoriteResponse

    private val _onDeleteResponse: MutableLiveData<Resource<Photo>> by lazy {
        MutableLiveData<Resource<Photo>>()
    }
    val onDeleteResponse get() = _onDeleteResponse

    fun insertPhotoToFavorite(photo: Photo) {
        viewModelScope.launch {
            _insertToFavoriteResponse.postValue(Resource.loading())
            try {
                repository.insert(photo.toPhotoDb())
                _insertToFavoriteResponse.postValue(Resource.success(true))
            } catch (e: Exception) {
                _insertToFavoriteResponse.postValue(Resource.error(e))
            }
        }
    }

    fun deletePhoto(photo: Photo) {
        viewModelScope.launch {
            _onDeleteResponse.postValue(Resource.loading())
            try {
                repository.delete(photo.toPhotoDb())
                _onDeleteResponse.postValue(Resource.success(photo))
            } catch (e: Exception) {
                _onDeleteResponse.postValue(Resource.error(e))
            }
        }
    }

}