package com.example.picturesapp.ui.fullscreen

import android.os.Bundle
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ShareCompat
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.example.picturesapp.R
import com.example.picturesapp.model.Photo
import com.example.picturesapp.ui.fullscreen.adapter.PhotoPagerAdapter
import com.example.picturesapp.utils.JsonUtils
import com.example.picturesapp.utils.Status
import com.example.picturesapp.utils.viewModelProvider
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class FullscreenActivity : DaggerAppCompatActivity(), ViewPager.OnPageChangeListener {
    private lateinit var viewPager: ViewPager
    private lateinit var favoriteImageButton: ImageButton
    private lateinit var infoImageButton: ImageButton
    private lateinit var shareImageButton: ImageButton
    private lateinit var backImageButton: ImageButton
    private lateinit var closeImageButton: ImageButton
    private lateinit var deleteImageButton: ImageButton
    private lateinit var titleTextView: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var bottomConstraintLayout: ConstraintLayout
    private lateinit var topConstraintLayout: ConstraintLayout
    private lateinit var rootLayout: ConstraintLayout
    private var adapter: PhotoPagerAdapter? = null
    private var photos = ArrayList<Photo>()
    private var position = 0
    private var fromFavorites = false

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: FullscreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen)

        viewModel = viewModelProvider(viewModelFactory)
        viewPager = findViewById(R.id.viewPager)
        favoriteImageButton = findViewById(R.id.favoriteImageButton)
        infoImageButton = findViewById(R.id.infoImageButton)
        shareImageButton = findViewById(R.id.shareImageButton)
        backImageButton = findViewById(R.id.backImageButton)
        closeImageButton = findViewById(R.id.closeImageButton)
        deleteImageButton = findViewById(R.id.deleteImageButton)
        titleTextView = findViewById(R.id.titleTextView)
        bottomConstraintLayout = findViewById(R.id.bottomConstraintLayout)
        topConstraintLayout = findViewById(R.id.topConstraintLayout)
        progressBar = findViewById(R.id.progressBar)
        rootLayout = findViewById(R.id.rootLayout)

        val photoStr = intent.getStringExtra(PHOTOS)
        position = intent.getIntExtra(POSITION, 0)
        fromFavorites = intent.getBooleanExtra(FROM_FAVORITES, false)
        photoStr?.let {
            photos.addAll(JsonUtils.parseJson(it))
        }
        setUpUi()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.insertToFavoriteResponse.observe(this, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    Snackbar.make(
                        rootLayout,
                        R.string.photo_added_to_favorites,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Snackbar.make(rootLayout, R.string.something_went_wrong, Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        })

        viewModel.onDeleteResponse.observe(this, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    Snackbar.make(
                        rootLayout,
                        R.string.photos_have_been_deleted,
                        Snackbar.LENGTH_SHORT
                    ).show()
                    adapter?.apply {
                        val index = photos.indexOfFirst { it.id == resource.data?.id }
                        photos.removeAt(index)
                        adapter?.notifyDataSetChanged()
                        if (photos.isEmpty())
                            finish()
                    }
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Snackbar.make(
                        rootLayout,
                        R.string.photos_could_not_delete_photos,
                        Snackbar.LENGTH_SHORT
                    )
                        .show()
                }
            }
        })
    }

    private fun setUpUi() {
        photos.let {
            adapter = PhotoPagerAdapter(it)
            adapter?.onClickLister = View.OnClickListener {
                toggle(
                    topConstraintLayout.visibility == View.GONE,
                    topConstraintLayout,
                    rootLayout,
                    Gravity.TOP
                )
            }
            viewPager.adapter = adapter
            viewPager.currentItem = position
        }
        viewPager.addOnPageChangeListener(this)

        favoriteImageButton.setOnClickListener {
            photos.let {
                viewModel.insertPhotoToFavorite(it[position])
            }
        }
        infoImageButton.setOnClickListener {
            toggle(
                bottomConstraintLayout.visibility == View.GONE,
                bottomConstraintLayout,
                rootLayout,
                Gravity.BOTTOM
            )
        }

        closeImageButton.setOnClickListener {
            toggle(
                false,
                bottomConstraintLayout,
                rootLayout,
                Gravity.BOTTOM
            )
        }
        shareImageButton.setOnClickListener {
            photos.let {
                shareUrl(it[position].url)
            }
        }
        backImageButton.setOnClickListener {
            finish()
        }
        deleteImageButton.setOnClickListener {
            photos.let { list ->
                viewModel.deletePhoto(list[position])
            }
        }
        deleteImageButton.visibility = if (fromFavorites) View.VISIBLE else View.GONE
        favoriteImageButton.visibility = if (!fromFavorites) View.VISIBLE else View.GONE
    }

    private fun shareUrl(url: String) {
        ShareCompat.IntentBuilder.from(this)
            .setType("text/plain")
            .setChooserTitle(getString(R.string.share_photo))
            .setText(url)
            .startChooser()
    }

    private fun toggle(show: Boolean, view: View, parent: ViewGroup, direction: Int) {
        val transition = Slide(direction)
        transition.duration = 600
        transition.addTarget(view)
        TransitionManager.beginDelayedTransition(parent, transition)
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    // RELEASE RESOURCE
    override fun onDestroy() {
        adapter = null
        photos.clear()
        viewPager.removeOnPageChangeListener(this)
        super.onDestroy()
    }

    companion object {
        const val PHOTOS = "PHOTOS"
        const val POSITION = "POSITION"
        const val FROM_FAVORITES = "FROM_FAVORITES"
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        this.position = position
        photos.let {
            setUpInfoPhoto(it[position])
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    private fun setUpInfoPhoto(photo: Photo) {
        titleTextView.text = photo.title
    }
}