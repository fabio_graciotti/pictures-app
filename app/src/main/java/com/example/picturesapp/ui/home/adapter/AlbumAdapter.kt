package com.example.picturesapp.ui.home.adapter

import android.content.Context
import android.view.ViewGroup
import com.example.picturesapp.model.Album
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter

class AlbumAdapter(context: Context) : RecyclerArrayAdapter<Album>(context) {
    override fun OnCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder<*> {
        return AlbumViewHolder(parent)
    }
}