package com.example.picturesapp.ui.favorites

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.picturesapp.model.PhotoDb
import com.example.picturesapp.repository.FavoritesRepository
import com.example.picturesapp.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(
    private val repository: FavoritesRepository
) : ViewModel() {

    private val _favoritesResponse: MutableLiveData<Resource<List<PhotoDb>>> by lazy {
        MutableLiveData<Resource<List<PhotoDb>>>()
    }
    val favoritesResponse get() = _favoritesResponse

    private val _onDeleteResponse: MutableLiveData<Resource<Boolean>> by lazy {
        MutableLiveData<Resource<Boolean>>()
    }
    val onDeleteResponse get() = _onDeleteResponse

    fun getAll() {
        viewModelScope.launch {
            _favoritesResponse.postValue(Resource.loading())
            try {
                val list = repository.getAll()
                _favoritesResponse.postValue(Resource.success(list))
            } catch (e: Exception) {
                _favoritesResponse.postValue(Resource.error(e))
            }
        }
    }

    fun deleteAll(list: List<PhotoDb>) {
        viewModelScope.launch {
            _onDeleteResponse.postValue(Resource.loading())
            try {
                repository.deleteAll(list)
                _onDeleteResponse.postValue(Resource.success(true))
            } catch (e: Exception) {
                _onDeleteResponse.postValue(Resource.error(e))
            }
        }
    }
}