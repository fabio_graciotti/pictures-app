package com.example.picturesapp.ui.gallery

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.picturesapp.R
import com.example.picturesapp.model.Photo
import com.example.picturesapp.ui.component.BottomGridItemDecoration
import com.example.picturesapp.ui.fullscreen.FullscreenActivity
import com.example.picturesapp.ui.gallery.adapter.PhotoAdapter
import com.example.picturesapp.utils.JsonUtils
import com.example.picturesapp.utils.Status
import com.example.picturesapp.utils.viewModelProvider
import com.jude.easyrecyclerview.EasyRecyclerView
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class GalleryFragment : DaggerFragment() {

    companion object {
        const val ALBUM_ID = "ALBUM_ID"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: GalleryViewModel
    private lateinit var recyclerView: EasyRecyclerView
    private var albumId: Int? = 0
    private var adapter: PhotoAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            albumId = getInt(ALBUM_ID, 0)
        }
        adapter = PhotoAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.gallery_fragment, container, false)
        recyclerView = root.findViewById(R.id.recyclerView)
        setUpUI()
        return root
    }

    private fun setUpUI() {
        recyclerView.setLayoutManager(
            GridLayoutManager(
                requireContext(),
                2,
                GridLayoutManager.VERTICAL,
                false
            )
        )
        recyclerView.addItemDecoration(BottomGridItemDecoration())

        adapter?.apply {
            setOnItemClickListener {
                goToFullscreen(it, allData)
            }
        }
        recyclerView.adapter = adapter
    }

    private fun goToFullscreen(position: Int, photos: List<Photo>) {
        val intent = Intent(requireContext(), FullscreenActivity::class.java)
        intent.putExtra(FullscreenActivity.PHOTOS, JsonUtils.photosToString(photos))
        intent.putExtra(FullscreenActivity.POSITION, position)
        startActivity(intent)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = viewModelProvider(viewModelFactory)
        setUpObservers()
        albumId?.let {
            viewModel.getPhoto(it)
        }
    }

    private fun setUpObservers() {
        viewModel.responsePhoto.observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.ERROR -> {
                    resource.throwable?.apply {
                        printStackTrace()
                    }
                    recyclerView.showError()
                }

                Status.LOADING -> recyclerView.showProgress()

                Status.SUCCESS -> {
                    resource.data?.let { response ->
                        if (response.isSuccessful) {
                            response.body()?.let { list ->
                                if (list.isNotEmpty())
                                    adapter?.apply {
                                        removeAll()
                                        addAll(list)
                                    }
                                else recyclerView.showEmpty()
                            }
                        } else recyclerView.showError()
                    }
                }
            }
        })
    }

    // RELEASE RESOURCE
    override fun onDestroy() {
        albumId = null
        adapter = null
        super.onDestroy()
    }

}