package com.example.picturesapp.ui.home

import androidx.lifecycle.*
import com.example.picturesapp.model.Album
import com.example.picturesapp.repository.HomeRepository
import com.example.picturesapp.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository
) : ViewModel() {

    private val _responseAlbums: MutableLiveData<Resource<Response<List<Album>>>> by lazy {
        MutableLiveData<Resource<Response<List<Album>>>>()
    }
    val responseAlbums get() = _responseAlbums

    init {
        getAlbums()
    }

    fun getAlbums() {
        _responseAlbums.postValue(Resource.loading())
        viewModelScope.launch {
            try {
                val resp = homeRepository.getAlbums()
                _responseAlbums.postValue(Resource.success(resp))
            } catch (e: Exception) {
                _responseAlbums.postValue(Resource.error(e))
            }
        }
    }
}