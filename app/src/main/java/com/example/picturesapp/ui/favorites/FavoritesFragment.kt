package com.example.picturesapp.ui.favorites

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.picturesapp.R
import com.example.picturesapp.model.Photo
import com.example.picturesapp.model.PhotoDb
import com.example.picturesapp.ui.favorites.adapter.FavoriteAdapter
import com.example.picturesapp.ui.fullscreen.FullscreenActivity
import com.example.picturesapp.utils.JsonUtils
import com.example.picturesapp.utils.Status
import com.example.picturesapp.utils.viewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.jude.easyrecyclerview.EasyRecyclerView
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class FavoritesFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var favoritesViewModel: FavoritesViewModel
    private lateinit var recyclerView: EasyRecyclerView
    private lateinit var controlsLayout: LinearLayout
    private lateinit var cancelButton: Button
    private lateinit var deleteButton: Button
    private var adapter: FavoriteAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = FavoriteAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_favorites, container, false)
        recyclerView = root.findViewById(R.id.recyclerView)
        controlsLayout = root.findViewById(R.id.controlsLayout)
        cancelButton = root.findViewById(R.id.cancelButton)
        deleteButton = root.findViewById(R.id.deleteButton)
        setUpUI()
        return root
    }

    private fun setUpUI() {
        adapter?.apply {
            setOnItemLongClickListener {
                if (!allData[it].isCheckable) {
                    setUpListCheckable(allData, true)
                    allData[it].isChecked = true
                    notifyItemChanged(it)
                    controlsLayout.visibility = View.VISIBLE
                }
                true
            }
            setOnItemClickListener {
                if (allData[it].isCheckable) {
                    allData[it].isChecked = !allData[it].isChecked
                    notifyItemChanged(it)
                } else goToFullscreen(it, allData.map { photoDb -> photoDb.toPhoto() })
            }
        }
        recyclerView.setLayoutManager(
            GridLayoutManager(
                requireContext(),
                2,
                GridLayoutManager.VERTICAL,
                false
            )
        )
        recyclerView.adapter = adapter

        cancelButton.setOnClickListener {
            adapter?.apply {
                setUpListCheckable(allData, false)
                controlsLayout.visibility = View.GONE
            }
        }

        deleteButton.setOnClickListener {
            adapter?.apply {
                val filteredList = allData.filter { it.isChecked }
                favoritesViewModel.deleteAll(filteredList)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        favoritesViewModel.getAll()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        favoritesViewModel = viewModelProvider(viewModelFactory)
        setUpObservers()
    }

    private fun setUpObservers() {
        favoritesViewModel.favoritesResponse.observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    adapter?.apply {
                        if (resource.data.isNullOrEmpty())
                            recyclerView.showEmpty()
                        else {
                            removeAll()
                            addAll(resource.data)
                        }
                    }
                }
                Status.ERROR -> recyclerView.showError()

                Status.LOADING -> recyclerView.showProgress()
            }
        })

        favoritesViewModel.onDeleteResponse.observe(viewLifecycleOwner, { resources ->
            when (resources.status) {
                Status.LOADING -> recyclerView.showProgress()
                Status.ERROR -> Snackbar.make(
                    requireView(),
                    R.string.photos_could_not_delete_photos,
                    Snackbar.LENGTH_SHORT
                ).show()
                Status.SUCCESS -> {
                    Snackbar.make(
                        requireView(),
                        R.string.photos_have_been_deleted,
                        Snackbar.LENGTH_SHORT
                    ).show()
                    adapter?.apply {
                        // setUpListCheckable(allData, false)
                        favoritesViewModel.getAll()
                        controlsLayout.visibility = View.GONE
                    }
                }
            }
        })
    }

    private fun goToFullscreen(position: Int, photos: List<Photo>) {
        val intent = Intent(requireContext(), FullscreenActivity::class.java)
        intent.putExtra(FullscreenActivity.POSITION, position)
        intent.putExtra(FullscreenActivity.FROM_FAVORITES, true)
        intent.putExtra(FullscreenActivity.PHOTOS, JsonUtils.photosToString(photos))
        startActivity(intent)
    }

    private fun setUpListCheckable(list: List<PhotoDb>, isCheckable: Boolean) {
        list.forEach {
            it.isCheckable = isCheckable
            it.isChecked = false
        }
        adapter?.apply {
            notifyDataSetChanged()
        }
    }

    override fun onDestroy() {
        adapter = null
        super.onDestroy()
    }

}