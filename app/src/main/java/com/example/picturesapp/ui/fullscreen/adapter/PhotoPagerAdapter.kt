package com.example.picturesapp.ui.fullscreen.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.picturesapp.R
import com.example.picturesapp.model.Photo
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso

class PhotoPagerAdapter(private val _photos: ArrayList<Photo>) : PagerAdapter() {
    val photos get() = _photos

    var onClickLister: View.OnClickListener? = null

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val root = inflater.inflate(R.layout.fullscreen_photo_layout, container, false)
        val photoView: PhotoView = root.findViewById(R.id.photoView)
        photoView.setOnClickListener(onClickLister)
        setData(_photos[position], photoView)
        container.addView(root)
        return root
    }

    private fun setData(data: Photo?, photoView: PhotoView) {
        data?.let { photo ->
            Picasso.get().load(photo.url).into(photoView)
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val photoView = container.findViewById<PhotoView>(R.id.photoView)
        photoView.scale = 1.0f
        container.removeView(`object` as View?)
    }

    override fun getCount(): Int {
        return _photos.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

}