package com.example.picturesapp.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.picturesapp.R
import com.example.picturesapp.ui.component.BottomItemDecoration
import com.example.picturesapp.ui.gallery.GalleryFragment
import com.example.picturesapp.ui.home.adapter.AlbumAdapter
import com.example.picturesapp.utils.Navigator
import com.example.picturesapp.utils.Status
import com.example.picturesapp.utils.viewModelProvider
import com.jude.easyrecyclerview.EasyRecyclerView
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class HomeFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var homeViewModel: HomeViewModel
    private var adapter: AlbumAdapter? = null
    private lateinit var recyclerView: EasyRecyclerView
    private var navigator: Navigator? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = context as? Navigator
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = AlbumAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerView = root.findViewById(R.id.recyclerView)
        setUpUI()
        return root
    }

    private fun setUpUI() {
        recyclerView.setLayoutManager(
            LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        recyclerView.addItemDecoration(BottomItemDecoration())
        recyclerView.setRefreshListener {
            homeViewModel.getAlbums()
        }
        adapter?.apply {
            setOnItemClickListener {
                navigator?.apply {
                    val args = Bundle()
                    args.putInt(GalleryFragment.ALBUM_ID, allData[it].id)
                    navigate(R.id.action_home_to_gallery, args)
                }
            }
        }
        recyclerView.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        homeViewModel = viewModelProvider(viewModelFactory)
        setUpObservers()
    }

    private fun setUpObservers() {
        homeViewModel.responseAlbums.observe(viewLifecycleOwner, { resource ->
            when (resource.status) {
                Status.ERROR -> {
                    resource.throwable?.apply {
                        printStackTrace()
                    }
                    recyclerView.showError()
                }

                Status.LOADING -> recyclerView.showProgress()

                Status.SUCCESS -> {
                    resource.data?.let { response ->
                        if (response.isSuccessful) {
                            response.body()?.let { list ->
                                if (list.isNotEmpty())
                                    adapter?.apply {
                                        removeAll()
                                        addAll(list)
                                    }
                                else recyclerView.showEmpty()
                            }
                        } else recyclerView.showError()
                    }
                }
            }
        })
    }

    // RELEASE RESOURCES
    override fun onDestroy() {
        adapter = null
        navigator = null
        super.onDestroy()
    }
}