package com.example.picturesapp.ui.gallery.adapter

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.picturesapp.R
import com.example.picturesapp.model.Photo
import com.jude.easyrecyclerview.adapter.BaseViewHolder
import com.squareup.picasso.Picasso

class PhotoViewHolder(parent: ViewGroup?) :
    BaseViewHolder<Photo>(parent, R.layout.photo_item_layout) {
    private val titleTextView: TextView
    private val imageView: ImageView

    init {
        titleTextView = `$`(R.id.titleTextView)
        imageView = `$`(R.id.imageView)
    }

    override fun setData(data: Photo?) {
        data?.let { photo ->
            titleTextView.text = photo.title
            Picasso.get().load(photo.thumbnailUrl).into(imageView)
        }
    }
}