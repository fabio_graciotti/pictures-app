package com.example.picturesapp.costants

import com.example.picturesapp.BuildConfig

object ServerDomain {
    // GET BASE URL FROM GRADLE CONFIGURATION
    const val BASE_URL = BuildConfig.BASE_URL

    const val ALBUMS = "albums"

    const val ALBUM_PHOTOS = "${ALBUMS}/{id}/photos"
}