package com.example.picturesapp.database

import androidx.room.*
import com.example.picturesapp.model.PhotoDb

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(photoDb: PhotoDb)

    @Delete
    suspend fun deleteAll(photos: List<PhotoDb>)

    @Query("SELECT * FROM favorite_photos")
    suspend fun getAll(): List<PhotoDb>

    @Delete
    suspend fun delete(photoDb: PhotoDb)
}