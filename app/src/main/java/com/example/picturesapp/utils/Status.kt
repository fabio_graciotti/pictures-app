package com.example.picturesapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}