package com.example.picturesapp.utils

import android.os.Bundle

interface Navigator {
    fun navigate(actionId: Int)
    fun navigate(actionId: Int, args: Bundle)
}