package com.example.picturesapp.utils

import com.example.picturesapp.model.Photo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object JsonUtils {
    fun photosToString(list: List<Photo>): String {
        return Gson().toJson(list)
    }

    fun parseJson(json: String): List<Photo> {
        val gson = Gson()
        val type: Type = object : TypeToken<List<Photo>>() {}.type
        return gson.fromJson(json, type)
    }
}