package com.example.picturesapp.repository

import com.example.picturesapp.database.PhotoDao
import com.example.picturesapp.model.PhotoDb
import javax.inject.Inject

class FavoritesRepository @Inject constructor(
    private val photoDao: PhotoDao
) {
    suspend fun deleteAll(photos: List<PhotoDb>) = photoDao.deleteAll(photos)

    suspend fun getAll() = photoDao.getAll()
}