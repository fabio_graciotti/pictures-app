package com.example.picturesapp.repository

import com.example.picturesapp.api.ApiInterface
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val apiInterface: ApiInterface
) {
    suspend fun getAlbums() = apiInterface.getAlbums()
}