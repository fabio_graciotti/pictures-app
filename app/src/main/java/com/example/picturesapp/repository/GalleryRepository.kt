package com.example.picturesapp.repository

import com.example.picturesapp.api.ApiInterface
import javax.inject.Inject

class GalleryRepository @Inject constructor(
    private val apiInterface: ApiInterface
) {
    suspend fun getPhotosByAlbum(albumId: Int) = apiInterface.getPhotosByAlbum(albumId)
}