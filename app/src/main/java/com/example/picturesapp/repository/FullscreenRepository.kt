package com.example.picturesapp.repository

import com.example.picturesapp.database.PhotoDao
import com.example.picturesapp.model.PhotoDb
import javax.inject.Inject

class FullscreenRepository @Inject constructor(
    private val photoDao: PhotoDao
) {
    suspend fun insert(photoDb: PhotoDb) = photoDao.insert(photoDb)

    suspend fun delete(photoDb: PhotoDb) = photoDao.delete(photoDb)
}