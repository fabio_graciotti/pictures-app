package com.example.picturesapp.module

import androidx.lifecycle.ViewModelProvider
import com.example.picturesapp.utils.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}