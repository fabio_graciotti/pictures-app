package com.example.picturesapp.module

import com.example.picturesapp.ui.favorites.FavoritesFragment
import com.example.picturesapp.ui.gallery.GalleryFragment
import com.example.picturesapp.ui.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {
    @ContributesAndroidInjector
    abstract fun provideHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun provideGalleryFragment(): GalleryFragment

    @ContributesAndroidInjector
    abstract fun provideFavoritesFragment(): FavoritesFragment
}