package com.example.picturesapp.module

import com.example.picturesapp.MainActivity
import com.example.picturesapp.ui.fullscreen.FullscreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindFullscreenActivity(): FullscreenActivity
}