package com.example.picturesapp.module

import androidx.lifecycle.ViewModel
import com.example.picturesapp.ui.favorites.FavoritesViewModel
import com.example.picturesapp.ui.fullscreen.FullscreenViewModel
import com.example.picturesapp.ui.gallery.GalleryViewModel
import com.example.picturesapp.ui.home.HomeViewModel
import com.example.picturesapp.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GalleryViewModel::class)
    internal abstract fun bindGalleryViewModel(galleryViewModel: GalleryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FullscreenViewModel::class)
    internal abstract fun bindFullscreenViewModel(fullscreenViewModel: FullscreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesViewModel::class)
    internal abstract fun bindFavoritesViewModel(favoritesViewModel: FavoritesViewModel): ViewModel
}