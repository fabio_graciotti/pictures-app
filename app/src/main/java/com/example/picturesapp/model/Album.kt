package com.example.picturesapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("id")
    @Expose
    private val _id: Int,

    @SerializedName("userId")
    @Expose
    private val _userId: Int,

    @SerializedName("title")
    @Expose
    private val _title: String
) {
    val id get() = _id
    val userId get() = _userId
    val title get() = _title
}
