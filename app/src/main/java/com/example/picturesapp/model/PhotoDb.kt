package com.example.picturesapp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey


@Entity(tableName = "favorite_photos")
data class PhotoDb(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "url")
    val url: String,

    @ColumnInfo(name = "thumbnailUrl")
    val thumbnailUrl: String,
) {

    @Ignore
    var isChecked = false


    @Ignore
    var isCheckable = false

    fun toPhoto(): Photo {
        return Photo(id, 0, title, url, thumbnailUrl)
    }
}
