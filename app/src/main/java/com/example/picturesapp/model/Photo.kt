package com.example.picturesapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Photo(
    @SerializedName("id")
    @Expose
    private val _id: Int,

    @SerializedName("albumId")
    @Expose
    private val _albumId: Int,

    @SerializedName("title")
    @Expose
    private val _title: String,

    @SerializedName("thumbnailUrl")
    @Expose
    private val _thumbnailUrl: String,

    @SerializedName("url")
    @Expose
    private val _url: String
) {
    val id get() = _id
    val albumId get() = _albumId
    val title get() = _title
    val thumbnailUrl get() = _thumbnailUrl
    val url get() = _url

    fun toPhotoDb(): PhotoDb {
        return PhotoDb(_id, _title, _url, _thumbnailUrl)
    }
}
