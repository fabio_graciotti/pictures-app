# READ ME

Pictures app consists in three sections:

 - **Home** where you can see all categories
 - **Gallery** reachable by clicking on a category in Home. where you can see all photo of a category
 - **Favorites** where you can see pictures saved as favorites
 - **FullScreen** reachable by clicking on a photo, in this section you can zoom-in and zoom-out, add picture to favorites (if you come from the home), remove picture from favorites (if you come from favorites).


The project is structured with the pattern Model-View-ViewModel.

# Technologies

The main technologies used in the project are listed below:

 - **Retrofit2**: To consume the REST services.
 - **Android Room**: To save data locally in an SQLite database. Android Room is very simple to use, by creating interfaces it is possible to define the queries, then it will be android room to implement these interfaces so as to be able to use the functions declared in the interfaces.
 - **Coroutines**: A coroutine is a concurrency design pattern that you can use on Android to simplify code that executes asynchronously.
 - **Dagger**:  Dagger help to automate dependency injection in the app.
 - **PhotoView**: this library extended ImageView, and allows you to zoom-in and zoom-out without conflicting with the ViewPager where it is inserted (as in this project)
 - **Picasso**: to display remote images
 - **Mockito**: to test viewModels